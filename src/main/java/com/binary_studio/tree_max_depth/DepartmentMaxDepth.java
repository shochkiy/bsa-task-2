package com.binary_studio.tree_max_depth;

import java.util.HashSet;
import java.util.Stack;

public final class DepartmentMaxDepth {

	static boolean areLeaf;
	static HashSet<Department> visitedNodes = new HashSet<>();
	static Stack<Department> route = new Stack<>();
	static int routeMaxSize;


	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		routeMaxSize = 0;
		if (rootDepartment == null) {
			return 0;
		}
		else {
			dfs(rootDepartment);
			return routeMaxSize+1;
		}
	}

	public static void dfs (Department root) {
		boolean moreThenOneChield = false;
		for (int i = 0; i < root.subDepartments.size(); i++) {
			if (root.subDepartments.get(i) != null) {
				while (!visitedNodes.contains(root.subDepartments.get(i))) {
					visitedNodes.add(root.subDepartments.get(i));
					route.add(root.subDepartments.get(i));
					while (!route.empty()) {
						Department p = route.peek();
						if (p.subDepartments.size() > 0) {
							for (int j = 0; j < p.subDepartments.size(); j++) {
								if (p.subDepartments.size() > 1) {
									moreThenOneChield = !visitedNodes.containsAll(p.subDepartments);
								}
								if (p.subDepartments.size() == 1) {
									moreThenOneChield = false;
								}
								if (!visitedNodes.contains(p.subDepartments.get(j))) {
									visitedNodes.add(p.subDepartments.get(j));
									route.add(p.subDepartments.get(j));
									p = p.subDepartments.get(j);
								}
								else if (!moreThenOneChield) {
									route.remove(p);
								}

							}
						}
						else {
							if (route.size()>routeMaxSize) {
								routeMaxSize = route.size();
							}
							route.remove(p);
						}
					}
				}
			}
		}
	}
}
