package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;
	private PositiveInteger shieldHP;
	private PositiveInteger hullHP;
	private PositiveInteger capacitorAmount;
	private PositiveInteger capacitorRechargeRate;
	private PositiveInteger speed;
	private PositiveInteger size;
	private AttackSubsystem attackSubsystem;
	private DefenciveSubsystem defenciveSubsystem;
	private int baseShieldHP;
	private int baseHullHP;
	private int baseCapacitorAmount;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
						PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.baseShieldHP = shieldHP.value();
		this.baseHullHP = hullHP.value();
		this.baseCapacitorAmount = capacitorAmount.value();

	}


	@Override
	public void endTurn() {
		this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value() + this.capacitorRechargeRate.value());
		if (this.capacitorAmount.value() > this.baseCapacitorAmount) {
			this.capacitorAmount = PositiveInteger.of(this.baseCapacitorAmount);
		}
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.capacitorAmount.value() >= this.attackSubsystem.getCapacitorConsumption().value()) {
			this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());
			return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
		}
		else {
			return Optional.empty();
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction attackAfterDefenciveSubsystem = this.defenciveSubsystem.reduceDamage(attack);
		if (this.shieldHP.value() >= attackAfterDefenciveSubsystem.damage.value()) {
			this.shieldHP = PositiveInteger.of(this.shieldHP.value() - attackAfterDefenciveSubsystem.damage.value());
		}
		else {
			if ((this.hullHP.value() - (attack.damage.value() - attackAfterDefenciveSubsystem.damage.value())) < 0) {
				this.hullHP = PositiveInteger.of(0);
			}
			else {
				this.hullHP = PositiveInteger.of(this.hullHP.value() - (attack.damage.value() - attackAfterDefenciveSubsystem.damage.value()));
			}
		}
		if (this.hullHP.value() > 0) {
			return new AttackResult.DamageRecived(attackAfterDefenciveSubsystem.weapon, attackAfterDefenciveSubsystem.damage, attackAfterDefenciveSubsystem.target);
		}
		else {
			return new AttackResult.Destroyed();
		}
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		int shieldHPFinalRegenerated = this.defenciveSubsystem.regenerate().shieldHPRegenerated.value();
		int hullHPFinalRegenerated = this.defenciveSubsystem.regenerate().hullHPRegenerated.value();
		if (this.capacitorAmount.value() >= this.defenciveSubsystem.getCapacitorConsumption().value()) {
			if (shieldHPFinalRegenerated >= (this.baseShieldHP - this.shieldHP.value())) {
				shieldHPFinalRegenerated = this.baseShieldHP - this.shieldHP.value();
				this.shieldHP = PositiveInteger.of(this.baseShieldHP);
			}
			else {
				this.shieldHP = PositiveInteger.of(this.shieldHP.value() + shieldHPFinalRegenerated);
			}
			if (hullHPFinalRegenerated >= (this.baseHullHP - this.hullHP.value())) {
				hullHPFinalRegenerated = this.baseHullHP - this.hullHP.value();
				this.hullHP = PositiveInteger.of(this.baseHullHP);
			}
			else {
				this.hullHP = PositiveInteger.of(this.hullHP.value() + hullHPFinalRegenerated);
			}
			this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
			return Optional.of(new RegenerateAction(PositiveInteger.of(shieldHPFinalRegenerated), PositiveInteger.of(hullHPFinalRegenerated)));
		}
		else {
			return Optional.empty();
		}
	}

}
